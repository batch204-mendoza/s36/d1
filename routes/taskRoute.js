// contains all the endpoints for our application
//  separate the routs such that "index.js" only conttains information on the server

const express = require("express");
// Router needed for setting up routes (function) allows us to access the http methods middleware that make it eassier to complete routes for the app
const router = express.Router();


const taskController= require("../controllers/taskController");

// routes

// route to get all the tasks

router.get("/",(req, res) => {
	//to be defined in controller 
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});
router.post("/", (req,res) =>{
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//params is from postman reserved word
router.delete("/:id", (req,res) =>{
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//route to update a task


router.put("/:id",(req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// use "module.exports to export the router object to use in the server= index.js file in this case"
module.exports = router;