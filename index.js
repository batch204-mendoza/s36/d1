// npm install module@verwsion number for getting specific version

// setup the dependencies
const express= require("express");
const mongoose= require("mongoose");

// this allows us to use all the routes defined in the "task route.js"
const taskRoute = require("./routes/taskRoute");

const app = express();
const port= 3000;

app.use(express.json());

// database connection
mongoose.connect("mongodb+srv://Jack:admin123@cluster0.o4i73.mongodb.net/B204-to-do?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => console.log(" We're connected to the cloud database"));

//adding task route

app.use("/tasks", taskRoute);
//adds /tasks to localhost:3000/ to make it  localhost:3000/tasks

app.listen(port,()=> console.log(`Now listening ot port ${port}`))